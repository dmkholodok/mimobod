# Lab work for MiMOBOD analyzing wine dataset using JDK, ClickHouse db, Tabix UI and Docker.
-------
## Usage
```
    docker-compose build
    docker-compose up
```
-------
Note: Tabix UI will be running at localhost:8080

