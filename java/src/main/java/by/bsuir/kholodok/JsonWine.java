package by.bsuir.kholodok;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.lang.reflect.Field;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonWine {

    private String country;
    private Integer points;
    private Integer price;
    private String variety;
    private String province;
    private String winery;
    private String title;

    @JsonProperty("taster_name")
    private String tasterName;


    public boolean hasAllFields() throws IllegalAccessException {
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value = field.get(this);
            if (value == null) {
                return false;
            }
        }
        return true;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getWinery() {
        return winery;
    }

    public void setWinery(String winery) {
        this.winery = winery;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTasterName() {
        return tasterName;
    }

    public void setTasterName(String tasterName) {
        this.tasterName = tasterName;
    }
}
