package by.bsuir.kholodok;

import java.sql.*;

public class ClickHouseClient implements DbClient {

    private static final String DB_URL = "jdbc:clickhouse://localhost:9000";
    private static final String ADD_QUERY = "insert into wines (" +
            "id," +
            "country," +
            "province," +
            "points," +
            "price," +
            "year," +
            "taster_name," +
            "variety," +
            "winery" +
            ") format Values (?,?,?,?,?,?,?,?,?)";

    private static final String CREATE_TABLE_QUERY = "create table if not exists wines (\n" +
            "event_date Date DEFAULT now(), \n" +
            "event_datetime DateTime DEFAULT now(),\n" +
            "id Int32,\n" +
            "country String,\n" +
            "province String,\n" +
            "points Int32,\n" +
            "price Int32,\n" +
            "year String,\n" +
            "taster_name String,\n" +
            "variety String,\n" +
            "winery String\n" +
            ") \n" +
            "ENGINE = MergeTree(event_date, (id), 8192)";

    private final static int BATCH_SIZE = 50000;

    private final PreparedStatement addStatement;
    private final Connection conn;

    private int batchCounter = 0;
    private int id = 1;


    public ClickHouseClient() throws SQLException, ClassNotFoundException {
        Class.forName("com.github.housepower.jdbc.ClickHouseDriver");
        conn = DriverManager.getConnection(DB_URL);
        addStatement = conn.prepareStatement(ADD_QUERY);
        createTableIfNotExists();
    }

    private void createTableIfNotExists() throws SQLException {
        var statement = conn.prepareStatement(CREATE_TABLE_QUERY);
        statement.execute();
        statement.close();
    }

    @Override
    public void add(WineDbRow row) {
        this.batchCounter++;
        try {
            addStatement.setInt(1, id++);
            addStatement.setString(2, row.getCountry());
            addStatement.setString(3, row.getProvince());
            addStatement.setInt(4, row.getPoints());
            addStatement.setInt(5, row.getPrice());
            addStatement.setString(6, row.getYear());
            addStatement.setString(7, row.getTasterName());
            addStatement.setString(8, row.getVariety());
            addStatement.setString(9, row.getWinery());
            addStatement.addBatch();
            if (batchCounter % BATCH_SIZE == 0) {
                this.batchCounter = 0;
                flush();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void flush() throws SQLException {
        addStatement.executeBatch();
    }

    @Override
    public void close() throws Exception {
        addStatement.close();
        conn.close();
    }
}
