package by.bsuir.kholodok;

import java.sql.SQLException;

public interface DbClient extends AutoCloseable {

    void add(WineDbRow row);
    void flush() throws SQLException;
}
