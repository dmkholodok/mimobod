package by.bsuir.kholodok;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WineDbRow {

    private final static Pattern NUMBER_PATTERN = Pattern.compile("[0-9]+");

    private String country;
    private Integer points;
    private Integer price;
    private String province;
    private String variety;
    private String winery;
    private String tasterName;
    private String year;

    public static WineDbRow of(JsonWine jsonWine) {
        WineDbRow wineDbRow = new WineDbRow();
        wineDbRow.setCountry(jsonWine.getCountry());
        wineDbRow.setPoints(jsonWine.getPoints());
        wineDbRow.setPrice(jsonWine.getPrice());
        wineDbRow.setProvince(jsonWine.getProvince());
        wineDbRow.setVariety(jsonWine.getVariety());
        wineDbRow.setWinery(jsonWine.getWinery());
        wineDbRow.setTasterName(jsonWine.getTasterName());
        wineDbRow.setYear(extractYearFromString(jsonWine.getTitle()));
        return wineDbRow;
    }

    private static String extractYearFromString(String str) {
        Matcher m = NUMBER_PATTERN.matcher(str);
        var list = new ArrayList<String>();
        while (m.find()) {
            list.add(m.group());
        }
        if (list.isEmpty()) {
            return "UNKNOWN";
        } else {
            for (String s : list) {
                if (s.length() > 3 && s.length() < 5) {
                    return s;
                }
            }
            return "UNKNOWN";
        }
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getWinery() {
        return winery;
    }

    public void setWinery(String winery) {
        this.winery = winery;
    }

    public String getTasterName() {
        return tasterName;
    }

    public void setTasterName(String tasterName) {
        this.tasterName = tasterName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
