package by.bsuir.kholodok;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static final String DS_FILENAME = "winemag-data-130k-v2.json";

    public static void main(String[] args) throws IOException, IllegalAccessException {

        var jsonWines = JsonReader.read(DS_FILENAME);

        var dbWineRows = new ArrayList<WineDbRow>(100000);
        for (var jsonWine : jsonWines) {
            if (jsonWine.hasAllFields()) {
                dbWineRows.add(WineDbRow.of(jsonWine));
            }
        }

        try(var dbClient = new ClickHouseClient()) {
            dbWineRows.forEach(dbClient::add);
            dbClient.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


